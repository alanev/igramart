var cardsCarousel = require('card/cards--carousel.js');
var introCarousel = require('intro/intro--carousel.js');
var popup = require('popup/popup.js');
var gallery = require('gallery/gallery.js');
var counter = require('counter/counter.js');

introCarousel.init();
cardsCarousel.init();
popup.init();
gallery.init();
counter.init();