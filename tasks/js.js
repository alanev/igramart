// modules
var gulp = require('gulp'),
	connect = require('gulp-connect'),

	// utils
	path = require('path'),
	plumber = require('gulp-plumber'),
	beep = require('./beep'),
	flatten = require('gulp-flatten'),
    connect = require('gulp-connect'),

	// js
	webpack = require('webpack'),
    glob = require('glob')
	;

// paths
var paths = require('./paths');;

// task
var tasks = function () {
    var files = {};
    glob.sync(
        path.join(
          process.cwd(), `${paths.src}*.js`
        )
      ).forEach(function (p) {
        files[path.parse(p).base] = p;
      });

	webpack({
        entry: files,
        output: {
            filename: '[name]',
            path: path.join(process.cwd(), paths.dest)
        },
        resolve: {
            root: [
                path.resolve('./modules')
            ]
        },
        module: {
            loaders: [{
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin()
        ]
	}, function (err, stats) {
        gulp.src(path.join(paths.dest, `*.js`))
            .pipe(connect.reload())
    });
    
    gulp.src(paths.modules + 'lib-*/*.js')
        .pipe(flatten())
        .pipe(gulp.dest(paths.dest + 'assets/libs/'))
        .pipe(connect.reload())
};

// module
module.exports = tasks;
