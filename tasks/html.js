// paths
var paths = require('./paths');

// modules
var gulp = require('gulp'),
	beep = require('./beep'),
	plumber = require('gulp-plumber'),
	connect = require('gulp-connect'),
    decode = require('ent/decode'),
    prettify = require('gulp-prettify'),

	postcss = require('gulp-postcss'),
	postxml = require('gulp-postxml'),
	plugins = [
		require('postxml-import')({
			path: function (attr) {
                if (!/(\\|\/|\.)/.test(attr)) {
                return 'modules/' + attr + '/' + attr + '.htm';
                }
				return attr;
			}
		}),
		require('postxml-beml')(),
		require('postxml-imgalt')(),
		require('postxml-placeholder')({
			url: 'http://placehold.alanev.ru/'
		}),
		require('postxml-image-size')({
			cwd: paths.dest
		}),
		require('postxml-wrap')(),
		require('postxml-repeat')(),
        (function () {
            return function($) {
                var ext = '.png';
                
                $('img[src]').each(function () {
                    var src = $(this).attr('src');
                    if (/\d+$/.test(src)) src += ext;
                    $(this).attr('src', src);
                });
                $('img[srcset]').each(function () {
                    var srcset = $(this).attr('src');
                    srcset.replace(/\d+ /g, function (char) {
                        return char + ext;
                    });
                    $(this).attr('srcset', srcset);
                });
            }
        })(),
        (function () {
            return function ($) {
                var html = $('html');
                $('html').html(decode(html.html()));
            }
        })()
	]
	;

// task
var task = function () {
	return gulp.src(`${paths.src}*.htm`)
		.pipe(plumber(beep))
		.pipe(postxml(plugins))
        .pipe(prettify())
		.pipe(plumber.stop())
		.pipe(gulp.dest(paths.dest))
		.pipe(connect.reload())
		;
}

// module
module.exports = task;
