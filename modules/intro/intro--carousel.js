const NL = require('../u-nl/u-nl.js');

module.exports = {
    previews: NL('.intro__content .gallery__preview'),
    articles: NL('.intro__article'),
    active: 0,
    toggle (index) {
        this.articles.item(this.active).classList.remove('_active');
        
        this.active = index;
        
        this.articles.item(this.active).classList.add('_active');
    },
    init () {
        if (this.previews) {
            this.previews.forEach((preview, index) => preview.on('click', () => this.toggle(index)));
        }
    }
};