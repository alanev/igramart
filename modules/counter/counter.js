const NL = require('../u-nl/u-nl.js');

module.exports = {
    elements: NL('.counter'),
    analyze (e) {
        
        let value = NL('.counter__value', this, true);
        let numb = Number(value.innerHTML);
        let clas = e.target.classList;
        
        if (clas.contains('counter__inc')) {
            value.innerHTML = ++numb;
        }
        if (clas.contains('counter__dec') && numb > 1) {
            value.innerHTML = --numb;
        }
    },
    init () {
        if (this.elements) {
            this.elements.on('click', this.analyze)
        }
    }
};