const NL = require('../u-nl/u-nl.js');

const popup = {
    activeClass: '_open',
    open (popupName) {
        popup.close();
        NL(`.popup[data-popup=${popupName}]`).classAdd(this.activeClass);
        document.body.classList.add('_lock');
    },
    closeAll () {
        popup.close();
        popup.removeHash();
    },
    close () {
        NL(`.popup.${popup.activeClass}`).classRemove(popup.activeClass);
        document.body.classList.remove('_lock');
    },
    removeHash () {
        history.pushState('', {}, location.pathname);
    },
    hasher () {
        let hash = location.hash.split('/');
        if (hash[0] === '#popup' && hash[1] != '') popup.open(hash[1]);
    },
    init () {
        window.addEventListener('hashchange', this.hasher);
        window.addEventListener('load', this.hasher);
        NL('.popup__close,.popup__overlay').on('click', this.closeAll);
    }
};
module.exports = popup