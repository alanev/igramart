const uNL = function (selector, container, only) {
    if (only) return (container || document).querySelector(selector);
    return (container || document).querySelectorAll(selector)
};
NodeList.prototype.forEach = Array.prototype.forEach;
NodeList.prototype.map = Array.prototype.map;
NodeList.prototype.on = function (type, cb) {
    this.forEach(item => item.addEventListener(type, cb, false))
}
NodeList.prototype.classRemove = function (className) {
    this.forEach(item => item.classList.remove(className))
};
NodeList.prototype.classAdd = function (className) {
    this.forEach(item => item.classList.add(className))
};
NodeList.prototype.classToggle =  function (className) {
    this.forEach(item => item.classList.toggle(className))
};
HTMLElement.prototype.on = HTMLElement.prototype.addEventListener;

module.exports = uNL;