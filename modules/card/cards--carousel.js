const NL = require('../u-nl/u-nl.js');

module.exports = {
    elements: NL('.cards__carousel'),
    // carousel: new Flickity(),
    init () {
        if (this.elements) {
            this.elements.forEach(element => new Flickity(element, {
                cellSelector: '.card',
                cellAlign: 'left',
                wrapAround: true,
                pageDots: false
            }))
        }
    }
};