const NL = require('../u-nl/u-nl.js');
const event = (index) => new Event('gallery', {index});

module.exports = {
    previews: NL('.gallery__preview'),
    images: NL('.gallery__img'),
    active: 0,
    toggle (index) {
        this.previews.item(this.active).classList.remove('_active');
        this.images.item(this.active).classList.remove('_active');
        
        this.active = index;
        
        this.previews.item(this.active).classList.add('_active');
        this.images.item(this.active).classList.add('_active');
    },
    init () {
        if (this.previews) {
            this.previews.forEach((preview, index) => preview.on('click', () => this.toggle(index)));
        }
    }
}